﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//40282244
//Jake Courtney
//10/12/16
//code for the invoice window
namespace CWRK2_40282244
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
       

        // this is all the txt, int and bools for booking window
        private string guest1name;
        private string guest2name;
        private string guest3name;
        private string guest4name;

        private string address;
        private string address2;
        private string address3;
        private string address4;

        private string days;

        bool breakfast = false;
        bool breakfast_2 = false;
        bool breakfast_3 = false;
        bool breakfast_4 = false;

        bool dinner = false;
        bool dinner_2 = false;
        bool dinner_3 = false;
        bool dinner_4 = false;

        private int guest_cost;
        private int get_sum;
        private int prices_of_extras;
       
        public Invoice()
        {
            InitializeComponent();
        }

        public void SetProperties(string guest1_name, string address_1, bool breakfast1, bool dinner1, bool carhire1,string guest2_name, bool breakfast2, bool dinner2, string guest2_address, int guestsCosts, string guest3_name, string guest3_address, bool breakfast3, bool dinner3, string guest4_name, string guest4_address, bool breakfast4, bool dinner4, int ExtrasPrices, int GetSum, string amountofdays)
        

        {
            this.guest1name = guest1_name;
            this.guest2name = guest2_name;
            this.guest3name = guest3_name;
            this.guest4name = guest4_name;

            this.address = address_1;
            this.address2 = guest2_address;
            this.address3 = guest3_address;
            this.address4 = guest4_address;

            this.breakfast = breakfast1;
            this.breakfast_2 = breakfast2;
            this.breakfast_3 = breakfast3;
            this.breakfast_4 = breakfast4;

            this.dinner = dinner1;
            this.dinner_2 = dinner2;
            this.dinner_3 = dinner3;
            this.dinner_4 = dinner4;

            this.guest_cost = guestsCosts;
            this.get_sum = GetSum;
            this.prices_of_extras = ExtrasPrices;
            this.days = amountofdays;
            

        }

        public void displayInvoice()

            {
                invoice_TextBox.Text = "Invoice for Guest(s)" + guest1name + " " + guest2name + " " + guest3name + " " + guest4name + " " + guest_cost + " From  guest(s)" + guest_cost + " Guest(s) added extras" + prices_of_extras + " " + get_sum + " total cost : " + FullPriceForBooking();
            }
        

        private void close_invoice_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public int FullPriceForBooking ()
        {
            int sum = get_sum;
            int day = int.Parse(days);
            int fullprice = (sum * day);
                return fullprice;
        }
   
    }
}
