﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//40282244
//Jake Courtney
//Booking class consisting all logic for booking
//10/12/16

namespace CWRK2_40282244
{
    [Serializable]
    class Bookings
    {   /// this is the txt for the main window


        // this is all the txt, int and bools for booking window
        public string guest1_name;
        public string guest2_name;
        public string guest3_name;
        public string guest4_name;

        private int guest1_passport_num;
        private int guest2_passport_num;
        private int guest3_passport_num;
        private int guest4_passport_num;
        private int cust1_ref_num;
    
        private int cust1_age;
        private int cust2_age;
        private int cust3_age;
        private int cust4_age;

        private DateTime start;
        private DateTime end;
        

        private string dietary1;
        private string dietary2;
        private string dietary3;
        private string dietary4;

        public bool add_guest1;
        public bool add_guest2;
        public bool add_guest3;
        public bool add_guest4;

        public bool car_hire1;
    
        public bool breakfast1;
        public bool breakfast2;
        public bool breakfast3;
        public bool breakfast4;
        private bool dinner1;
        private bool dinner2;
        private bool dinner3;
        private bool dinner4;

        public bool Guest2_under18;
        public bool Guest3_under18;
        public bool Guest4_under18;

        public string address1;
        public string address2;
        public string address3;
        public string address4;

        public int GetCost;
        public int AddedExtras;
        public int Price;
        public int cust_ref_num;
        public int booking_ref_num;

        private int bookrefnumb;
        private int custrefnumb;

        public Bookings()
        {
            add_guest1 = false;
            add_guest2 = false;
            add_guest3 = false;
            add_guest4 = false;

            car_hire1 = false;
         

            breakfast1 = false;
            breakfast2 = false;
            breakfast3 = false;
            breakfast4 = false;

            dinner1 = false;
            dinner2 = false;
            dinner3 = false;
            dinner4 = false;



        }


        public bool Add_guest1
        {
            get
            {
                return add_guest1;
            }
            set
            {

                add_guest1 = value;
            }
        }


        public bool Add_guest2
        {
            get
            {
                return add_guest2;
            }
            set
            {

                add_guest2 = value;
            }
        }


        public bool Add_guest3
        {
            get
            {
                return add_guest3;
            }
            set
            {

                add_guest3 = value;
            }
        }


        public bool Add_guest4
        {
            get
            {
                return add_guest4;
            }
            set
            {

                add_guest4 = value;
            }
        }

        public bool Car_hire1
        {
            get
            {
                return car_hire1;
            }
            set
            {

                car_hire1 = value;
            }
        }


        public bool Breakfast1
        {
            get
            {
                return breakfast1;
            }
            set
            {

                breakfast1 = value;
            }
        }


        public bool Breakfast2
        {
            get
            {
                return breakfast2;
            }
            set
            {

                breakfast2 = value;
            }
        }

        public bool Breakfast3
        {
            get
            {
                return breakfast3;
            }
            set
            {

                breakfast3 = value;
            }
        }


        public bool Breakfast4
        {
            get
            {
                return breakfast4;
            }
            set
            {

                breakfast4 = value;
            }
        }


        public bool Dinner1
        {
            get
            {
                return dinner1;
            }
            set
            {

                dinner1 = value;
            }
        }


        public bool Dinner2
        {
            get
            {
                return dinner2;
            }
            set
            {

                dinner2 = value;
            }
        }


        public bool Dinner3
        {
            get
            {
                return dinner3;
            }
            set
            {

                dinner3 = value;
            }
        }


        public bool Dinner4
        {
            get
            {
                return dinner4;
            }
            set
            {

                dinner4 = value;
            }
        }





       
      

       

        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    throw new ArgumentException("Must enter address");
                }
                else
                {
                    address1 = value;
                }
            }
        }

        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    throw new ArgumentException("Must enter address");
                }
                else
                {
                    address2 = value;
                }
            }
        }

        public string Address3
        {
            get
            {
                return address3;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    throw new ArgumentException("Must enter address");
                }
                else
                {
                    address3 = value;
                }
            }
        }

        public string Address4
        {
            get
            {
                return address4;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    throw new ArgumentException("Must enter address");
                }
                else
                {
                    address4 = value;
                }
            }
        }

        public int Cust1_ref_num
        {
            get
            {
                return cust_ref_num;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Must enter valid customer Refrence number or register with us to book now");
                }
                else
                {
                    cust_ref_num = value;
                }
            }
        }

        //Booking window

        public string guest1_Name
        {
            get
            {
                return guest1_name;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    throw new ArgumentException("Must fill in name as the main guest");
                }
                else
                {
                    guest1_name = value;
                }
            }
        }

        public string Guest2_Name
        {
            get
            {
                return guest2_name;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guest2_name = value;
                }
                else
                {
                    guest2_name = value;
                }
            }
        }

        public string Guest3_Name
        {
            get
            {
                return guest3_name;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guest3_name = value;
                }
                else
                {
                    guest3_name = value;
                }
            }
        }

        public string Guest4_Name
        {
            get
            {
                return guest4_name;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    guest4_name = value;
                }
                else
                {
                    guest4_name = value;
                }
            }
        }


        public int Guest1_Passport_Num
        {
            get
            {
                return guest1_passport_num;
            }
            set
            {
                if (value == null)
                {
                    guest1_passport_num = value;
                }
                else
                {
                    guest1_passport_num = value;
                }
            }
        }

        public int Guest2_Passport_Num
        {
            get
            {
                return guest2_passport_num;
            }
            set
            {
                if (value == null)
                {
                    guest2_passport_num = value;
                }
                else
                {
                    guest2_passport_num = value;
                }
            }
        }

        public int Guest3_Passport_Num
        {
            get
            {
                return guest3_passport_num;
            }
            set
            {
                if (value == null)
                {
                    guest3_passport_num = value;
                }
                else
                {
                    guest3_passport_num = value;
                }
            }
        }
        public int Guest4_Passport_Num
        {
            get
            {
                return guest4_passport_num;
            }
            set
            {
                if (value == null)
                {
                    guest4_passport_num = value;
                }
                else
                {
                    guest4_passport_num = value;
                }
            }
        }

   
     public int Cust1_Age
        {
            get
            {
                return cust1_age;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Please Enter a Value ");
                }
                if (value < 0)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                if (value > 101)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                else
                {
                    cust1_age = value;
                }
            }
        }

        public int Cust2_Age
        {
            get
            {
                return cust2_age;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Please Enter a Value");
                }
                if (value < 0)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                if (value > 101)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                else
                {
                    cust2_age = value;
                }
            }
        }

        public int cust3_Age
        {
            get
            {
                return cust3_age;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Please Enter a Value");
                }
                if (value < 0)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                if (value > 101)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                else
                {
                    cust3_age = value;
                }
            }
        }

        public int Cust4_Age
        {
            get
            {
                return cust4_age;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("Please Enter a Value");
                }
                if (value < 0)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                if (value > 101)
                {
                    throw new ArgumentException("Please Enter a valid Value");
                }
                else
                {
                    cust4_age = value;
                }
            }
        }

        public string Dietary1
        {
            get
            {
                return dietary1;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    dietary1 = value;
                }
                else
                {
                    dietary1 = value;
                }
            }
        }


        public string Dietary2
        {
            get
            {
                return dietary2;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    dietary2 = value;
                }
                else
                {
                    dietary2 = value;
                }
            }
        }

        public string Dietary3
        {
            get
            {
                return dietary3;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    dietary3 = value;
                }
                else
                {
                    dietary3 = value;
                }
            }
        }

        public string Dietary4
        {
            get
            {
                return dietary4;
            }
            set
            {
                if (value == String.Empty || value == null)
                {
                    dietary4 = value;
                }
                else
                {
                    dietary4 = value;
                }
            }
        }

        public int GuestsCosts()
        {
            int value = 0;
            if (add_guest1)
            {
                return (int)(value + 50);
            }
            if (add_guest2)
            {
                return (int)(value + 100);
            }
            if (add_guest3)
            {
                return (int)(value + 150);
            }
            if (add_guest4)
            {
                return (int)(value + 200);
            }
            else
            {
                return value;
            }                   
        }

        public int ExtrasPrices()
        {
            int added_value = 0;
            
            if (car_hire1)
            {
                return (int)(added_value + 50);
            }
            if (breakfast1)
            {
                return (int)(added_value + 5);
            }
            if (breakfast2)
            {
                return (int)(added_value + 5);
            }
            if (breakfast3)
            {
                return (int)(added_value + 5);
            }
            if (breakfast4)
            {
                return (int)(added_value + 5);
            }
            if (dinner1)
            {
                return (int)(added_value + 15);
            }
            if (dinner2)
            {
                return (int)(added_value + 15);
            }
            if (dinner3)
            {
                return (int)(added_value + 15);
            }
            if (dinner4)
            {
                return (int)(added_value + 15);
            }
            else
            {
                return added_value;
            }
        }

        public int GetSum()
        {
            int getcost = GuestsCosts();
            int addedextras = ExtrasPrices();
            int Price = (getcost + addedextras);
            return Price;
        }
        public DateTime StartDate
        {
            get
            {
                return start;
            }
            set
            { 
                if(value == null)
                {
                    throw new ArgumentException("Please, insert start date");
                }
                else
                {
                    start = value;
                }
                }
        }
        public DateTime EndDate
        {
            get
            {
                return end;
            }
            set
            {
                if(value == null)
                {
                    throw new ArgumentException("You need to insert end date");
                }
                else
                {
                    end = value;
                }
            }
        }
        public int GenBook 
        {
            get { return bookrefnumb; }
            set { bookrefnumb = value; }
        }
        public int GenCusRefNum
        {
            get { return custrefnumb; }
            set { custrefnumb = value; }
        }
    }
}
