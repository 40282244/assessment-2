﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CWRK2_40282244
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Bookings bookings; 
        public MainWindow()
        {
            bookings = new Bookings();

            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
            //enter button
        {
            CustomerAccount accountWindow = new CustomerAccount();
            accountWindow.Show();
        }

        private void ExitMain_Click(object sender, RoutedEventArgs e)
            //exit button
        {
            Close();
        }

        private void RegisterMain_Click(object sender, RoutedEventArgs e)
            //register button
        {
            Booking windowBooking = new Booking();
            windowBooking.Show();
        }


       
    }

}
