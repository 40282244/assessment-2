﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//40282244
//Jake Courtney
//10/12/16
//code for the bookings window 
namespace CWRK2_40282244
{
    /// <summary>
    /// Interaction logic for Booking.xaml
    /// </summary>
    public partial class Booking : Window
    {
        Bookings bookings;


        public Booking()
        {
            bookings = new Bookings();


            InitializeComponent();

            guest1_name.Visibility = Visibility.Visible;
            guest2_name.Visibility = Visibility.Hidden;
            guest3_name.Visibility = Visibility.Hidden;
            guest4_name.Visibility = Visibility.Hidden;

            guest1_passport_num.Visibility = Visibility.Visible;
            guest2_passport_num.Visibility = Visibility.Hidden;
            guest3_passport_num.Visibility = Visibility.Hidden;
            guest4_passport_num.Visibility = Visibility.Hidden;

            cust1_age.Visibility = Visibility.Visible;
            cust2_age.Visibility = Visibility.Hidden;
            cust3_age.Visibility = Visibility.Hidden;
            cust4_age.Visibility = Visibility.Hidden;

            Guest2_under18.Visibility = Visibility.Hidden;
            Guest3_under18.Visibility = Visibility.Hidden;
            Guest4_under18.Visibility = Visibility.Hidden;

            dietary1.Visibility = Visibility.Visible;
            dietary2.Visibility = Visibility.Hidden;
            dietary3.Visibility = Visibility.Hidden;
            dietary4.Visibility = Visibility.Hidden;
            
        }

        private void GenCusRefNum_Click(object sender, RoutedEventArgs e)
            //it creates random number for customer
        {
            Random custrnd = new Random();
            int custrndvalue = custrnd.Next(999,2000);
            cust_ref_num.Text = custrndvalue.ToString();
        }

        private void GenBook_Click(object sender, RoutedEventArgs e)
        {//it creates random number for customer booking
            Random bookrnd = new Random();
            int bookrndvalue = bookrnd.Next(999, 2000);
            booking_ref_num.Text = bookrndvalue.ToString();
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Book_Button_Click(object sender, RoutedEventArgs e)
        {
            guest1_name.Text = bookings.guest1_name;
            CustomerAccount customeraccount = new CustomerAccount();
            customeraccount.Show();


            bookings.guest1_name = guest1_name.Text;
            address1.Text = bookings.address1;
            address2.Text = bookings.address2;
            address3.Text = bookings.address3;
            address4.Text = bookings.address4;
            guest1_name.Text = bookings.guest1_name;
            guest2_name.Text = bookings.guest2_name;
            guest3_name.Text = bookings.guest3_name;
            guest4_name.Text = bookings.guest4_name;
            //guest checkboxes
            bookings.add_guest1 = (bool)add_guest1.IsChecked;
            bookings.add_guest2 = (bool)add_guest2.IsChecked;
            bookings.add_guest3 = (bool)add_guest3.IsChecked;
            bookings.add_guest4 = (bool)add_guest4.IsChecked;
        

            bookings.GenBook = int.Parse(booking_ref_num.Text);
            bookings.GenCusRefNum = int.Parse(cust_ref_num.Text);
            //checkboxes for extras
            bookings.breakfast1 = (bool)breakfast1.IsChecked;
            bookings.breakfast2 = (bool)breakfast2.IsChecked;
            bookings.breakfast3 = (bool)breakfast3.IsChecked;
            bookings.breakfast4 = (bool)breakfast4.IsChecked;
            bookings.Dinner1 = (bool)dinner1.IsChecked;
            bookings.Dinner2 = (bool)dinner2.IsChecked;
            bookings.Dinner3 = (bool)dinner3.IsChecked;
            bookings.Dinner4 = (bool)dinner4.IsChecked;



            customeraccount.SetProperties(bookings.guest1_name, bookings.guest2_name, bookings.guest3_name, bookings.guest4_name, bookings.address1, bookings.address2, bookings.address3, bookings.address4, bookings.GuestsCosts(), bookings.Dinner1, bookings.Dinner2, bookings.Dinner3, bookings.Dinner4, bookings.car_hire1, bookings.breakfast1, bookings.breakfast2, bookings.breakfast3, bookings.breakfast4, bookings.Guest2_under18, bookings.Guest3_under18, bookings.ExtrasPrices(), bookings.GetSum(), bookings.GenCusRefNum, bookings.GenBook);
            customeraccount.DisplayCustomerAccount();
        }

        private void add_guest1_Checked(object sender, RoutedEventArgs e)
        {
           
        }

        private void add_guest2_Checked(object sender, RoutedEventArgs e)
        {
            if (true)
            {
                guest1_name.Visibility = Visibility.Visible;
                guest2_name.Visibility = Visibility.Visible;
              

                guest1_passport_num.Visibility = Visibility.Visible;
                guest2_passport_num.Visibility = Visibility.Visible;
               

                cust1_age.Visibility = Visibility.Visible;
                cust2_age.Visibility = Visibility.Visible;
                

                Guest2_under18.Visibility = Visibility.Visible;
     
            }

        }

        private void addGuest3_Checked(object sender, RoutedEventArgs e)
        {
            if (true)
            {
                guest1_name.Visibility = Visibility.Visible;
                guest2_name.Visibility = Visibility.Visible;
                guest3_name.Visibility = Visibility.Visible;


                guest1_passport_num.Visibility = Visibility.Visible;
                guest2_passport_num.Visibility = Visibility.Visible;
                guest3_passport_num.Visibility = Visibility.Visible;


                cust1_age.Visibility = Visibility.Visible;
                cust2_age.Visibility = Visibility.Visible;
                cust3_age.Visibility = Visibility.Visible;


                Guest2_under18.Visibility = Visibility.Visible;
                Guest3_under18.Visibility = Visibility.Visible;
            }

        }
        private void addGuest4_Checked(object sender, RoutedEventArgs e)
        {
            if (true)
            {
                guest1_name.Visibility = Visibility.Visible;
                guest2_name.Visibility = Visibility.Visible;
                guest3_name.Visibility = Visibility.Visible;
                guest4_name.Visibility = Visibility.Visible;

                guest1_passport_num.Visibility = Visibility.Visible;
                guest2_passport_num.Visibility = Visibility.Visible;
                guest3_passport_num.Visibility = Visibility.Visible;
                guest4_passport_num.Visibility = Visibility.Visible;

                cust1_age.Visibility = Visibility.Visible;
                cust2_age.Visibility = Visibility.Visible;
                cust3_age.Visibility = Visibility.Visible;
                cust4_age.Visibility = Visibility.Visible;

                Guest2_under18.Visibility = Visibility.Visible;
                Guest3_under18.Visibility = Visibility.Visible;
                Guest4_under18.Visibility = Visibility.Visible;
            }
        }

        private void guest1_name_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void booking_ref_num_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
            //invoice
        {
            Invoice windowInv = new Invoice();
            windowInv.Show();

            bookings.guest1_Name = guest1_name.Text;
            bookings.Guest2_Name = guest2_name.Text;
            bookings.Guest3_Name = guest3_name.Text;
            bookings.Guest4_Name = guest4_name.Text;

            //adding guest check boxes
            bookings.Add_guest1 = (bool)add_guest1.IsChecked;
            bookings.Add_guest2 = (bool)add_guest2.IsChecked;
            bookings.Add_guest3 = (bool)add_guest3.IsChecked;
            bookings.Add_guest4 = (bool)add_guest4.IsChecked;

            //adding extra check boxes
            bookings.Breakfast1 = (bool)breakfast1.IsChecked;
            bookings.Breakfast2 = (bool)breakfast2.IsChecked;
            bookings.Breakfast3 = (bool)breakfast3.IsChecked;
            bookings.Breakfast4 = (bool)breakfast4.IsChecked;

            bookings.Dinner1 = (bool)dinner1.IsChecked;
            bookings.Dinner2 = (bool)dinner2.IsChecked;
            bookings.Dinner3 = (bool)dinner3.IsChecked;
            bookings.Dinner4 = (bool)dinner4.IsChecked;

            bookings.Car_hire1 = (bool)car_hire1.IsChecked;

            //dates

            DateTime date1 = StartDate.SelectedDate.Value.Date;
            DateTime enddate1 = EndDate.SelectedDate.Value.Date;
            TimeSpan days = enddate1.Subtract(date1);
            string money = days.TotalDays.ToString();

            windowInv.SetProperties(bookings.guest1_Name, bookings.address1, bookings.Breakfast1, bookings.Dinner1, bookings.Car_hire1, bookings.Guest2_Name, bookings.Breakfast2, bookings.Dinner2, bookings.address2, bookings.GuestsCosts(), bookings.Guest3_Name, bookings.address3, bookings.Breakfast3, bookings.Dinner3, bookings.Guest4_Name, bookings.address4, bookings.Breakfast4, bookings.Dinner4, bookings.ExtrasPrices(), bookings.GetSum(), money);
            windowInv.displayInvoice();
        }
      
                 
        
       
      

     

      

       

      

       

       
    }
}
