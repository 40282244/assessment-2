﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//40282244
//Jake Courntey
//code for window presenting customer account
//10/12/2016

namespace CWRK2_40282244
{
    /// <summary>
    /// Interaction logic for CustomerAccount.xaml
    /// </summary>
    public partial class CustomerAccount : Window
    {
        private string guest1_name;
        private string guest2_name;
        private string guest3_name;
        private string guest4_name;

        private string address;
        private string address2;
        private string address3;
        private string address4;

        private int customerreference;
        private int bookreference;

        bool breakfast = false;
        bool breakfast_2 = false;
        bool breakfast_3 = false;
        bool breakfast_4 = false;

        bool dinner = false;
        bool dinner_2 = false;
        bool dinner_3 = false;
        bool dinner_4 = false;

        private int get_sum;
        private int prices_of_extras;
       


        public CustomerAccount()
        {
            InitializeComponent();
        }



        private void Exit_Click(object sender, RoutedEventArgs e)
            //closing button
        {
            this.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
            //it saves booking
        {
            SaveFileDialog savefile1 = new SaveFileDialog();
            savefile1.FileName = "";
            savefile1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
          
            if(savefile1.ShowDialog() == true)
            {
                using (Stream streamst = File.Open(savefile1.FileName, FileMode.CreateNew))
                using(StreamWriter sw = new StreamWriter(streamst))
                {
                    sw.Write(TextBoxInfo.Text);
                }
            }
        }

        private void Show_Click(object sender, RoutedEventArgs e)
            //it loads on the screen custemer booking
        {
            OpenFileDialog load = new OpenFileDialog();
           
            string loadfile = bookingloadtextbox.ToString();
            string loaddirectory = custrefloadtextbox.ToString();
            load.FileName = loadfile;
            load.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (load.ShowDialog() == true)
            {
                TextBoxInfo.Text = File.ReadAllText(load.FileName);
            }
        }

        public void SetProperties(string guest1name, string guest2name, string guest3name, string guest4name, string address1, string address2, string address3, string address4, int guestscost, bool dinner1, bool dinner2, bool dinner3, bool dinner4, bool carhire1, bool breakfast1, bool breakfast2, bool breakfast3, bool breakfast4, bool guest2under18, bool guest3under18, int extraprices, int getsum, int custrefnumb, int bookingrefnumb)
        {
            this.guest1_name = guest1name;
            this.guest2_name = guest2name;
            this.guest3_name = guest3name;
            this.guest4_name = guest4name;

            this.address = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.address4 = address4;

            this.breakfast = breakfast1;
            this.breakfast_2 = breakfast2;
            this.breakfast_3 = breakfast3;
            this.breakfast_4 = breakfast4;

            this.dinner = dinner1;
            this.dinner_2 = dinner2;
            this.dinner_3 = dinner3;
            this.dinner_4 = dinner4;

            this.get_sum = getsum;
            this.prices_of_extras = extraprices;

            this.customerreference = custrefnumb;
            this.bookreference = bookingrefnumb;
        }
        public void DisplayCustomerAccount ()
        {
            TextBoxInfo.Text = "Customer Reference Number " + customerreference +" Customer Booking Reference Number " + bookreference +" Details: " + guest1_name + ", " + guest2_name + " , " + guest3_name + " , " + guest4_name + " , " + address + " , " + address2 + " , " + address3 + " , " + address4;
        }
    }
}
